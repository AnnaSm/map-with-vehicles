import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { VehicleWrapper } from './VehicleWrapper';

@Injectable({
  providedIn: 'root'
})
export class VehicleService {
  url = 'https://dev.vozilla.pl/api-client-portal/map?objectType=VEHICLE';

  constructor(private http: HttpClient) { }

  getVehicles(): Observable<VehicleWrapper> {
    return this.http.get<VehicleWrapper>(this.url);
  }
}
