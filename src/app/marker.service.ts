import { Injectable } from '@angular/core';
import * as L from 'leaflet';
import { PopupService } from './popup.service';
import "leaflet.markercluster"
import { Vehicle } from './Vehicle';

@Injectable({
  providedIn: 'root'
})
export class MarkerService {

  constructor(private popupService: PopupService) {
  }

  makeVehiclesMarkers(map: L.Map, vehicles: Vehicle[]): void {
    const markers = L.markerClusterGroup();
    for (const c of vehicles) {
      const lat = c.location.latitude;
      const lon = c.location.longitude;
      const marker = L.marker([lat, lon]);
      var iconSymbol = '';

      if (c.type === 'CAR') {
        if (c.status === 'AVAILABLE') {
          iconSymbol = 'assets/markers/marker-green-car.png';
        }
        else { iconSymbol = 'assets/markers/marker-unavailable-car.png'; }
      } else if (c.type === 'TRUCK') {
        if (c.status === 'AVAILABLE') {
          iconSymbol = 'assets/markers/marker-green-truck.png';
        }
        else { iconSymbol = 'assets/markers/marker-unavailable-truck.png'; }
      }

      var iconCustom = L.icon({
        iconUrl: iconSymbol,
        shadowUrl: 'assets/markers/marker-shadow.png',
        iconSize: [32, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        tooltipAnchor: [16, -28],
        shadowSize: [41, 41]
      });

      marker.bindPopup(this.popupService.makeVehiclesPopup(c));
      marker.setIcon(iconCustom);
      markers.addLayer(marker);
    }
    map.addLayer(markers);
  }
}
