import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { FilterService } from '../filter.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {

  form = new FormGroup({
    type: new FormControl('allType'),
    status: new FormControl('allStatus'),
    battery: new FormControl('allBatteryLevel')
  });

  constructor(private filterService: FilterService,) { }

  ngOnInit(): void {
  }

  submit() {
    console.log(this.form.value);
  }

  changeType(e: any) {
    switch (e.target.value) {
      case "allType": {
        this.filterService.includeCar = true;
        this.filterService.includeTruck = true;
        break;
      }
      case "car": {
        this.filterService.includeCar = true;
        this.filterService.includeTruck = false;
        break;
      }
      case "truck": {
        this.filterService.includeCar = false;
        this.filterService.includeTruck = true;
        break;
      }
    }
  }

  changeStatus(e: any) {
    switch (e.target.value) {
      case "allStatus": {
        this.filterService.includeAvailable = true;
        this.filterService.includeUnavailable = true;
        break;
      }
      case "available": {
        this.filterService.includeAvailable = true;
        this.filterService.includeUnavailable = false;
        break;
      }
      case "unavailable": {
        this.filterService.includeAvailable = false;
        this.filterService.includeUnavailable = true;
        break;
      }
    }
  }

  changeBattery(e: any) {
    switch (e.target.value) {
      case "allBatteryLevel": {
        this.filterService.includeLowBattery = true;
        this.filterService.includeMediumBattery = true;
        this.filterService.includeHighBattery = true;
        break;
      }
      case "lowBatteryLevel": {
        this.filterService.includeLowBattery = true;
        this.filterService.includeMediumBattery = false;
        this.filterService.includeHighBattery = false;
        break;
      }
      case "mediumBatteryLevel": {
        this.filterService.includeLowBattery = false;
        this.filterService.includeMediumBattery = true;
        this.filterService.includeHighBattery = false;
        break;
      }
      case "highBatteryLevel": {
        this.filterService.includeLowBattery = false;
        this.filterService.includeMediumBattery = false;
        this.filterService.includeHighBattery = true;
        break;
      }
    }
  }

}
