import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PopupService {

  constructor() { }

  makeVehiclesPopup(data: any): string {
    return `` +
      `<div>Color: ${data.color}</div>` +
      `<div>Type: ${data.type}</div>` +
      `<div>Plates Number: ${data.platesNumber}</div>` +
      `<div>Battery Level: ${data.batteryLevelPct}%</div>` +
      `<div>Status: ${data.status}</div>`
  }
}
