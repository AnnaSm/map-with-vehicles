import { Injectable } from '@angular/core';
import { Vehicle } from './Vehicle';

@Injectable({
  providedIn: 'root'
})
export class FilterService {

  includeCar: boolean = true;
  includeTruck: boolean = true;
  includeAvailable: boolean = true;
  includeUnavailable: boolean = true;
  includeLowBattery: boolean = true;
  includeMediumBattery: boolean = true;
  includeHighBattery: boolean = true;

  constructor() { }

  filterVehicles(vehicles: Vehicle[]): Vehicle[] {
    if (!this.includeCar)
      vehicles = vehicles.filter(this.excludeCar);
    if (!this.includeTruck)
      vehicles = vehicles.filter(this.excludeTruck);
    if (!this.includeAvailable)
      vehicles = vehicles.filter(this.excludeAvailable);
    if (!this.includeUnavailable)
      vehicles = vehicles.filter(this.excludeUnavailable);
    if (!this.includeLowBattery)
      vehicles = vehicles.filter(this.excludeLowBattery);
    if (!this.includeMediumBattery)
      vehicles = vehicles.filter(this.excludeMediumBattery);
    if (!this.includeHighBattery)
      vehicles = vehicles.filter(this.excludeHighBattery);
    return vehicles;
  }

  excludeCar(element: Vehicle, index: any, array: any) {
    return (element.type != "CAR");
  }
  excludeTruck(element: Vehicle, index: any, array: any) {
    return (element.type != "TRUCK");
  }
  excludeAvailable(element: Vehicle, index: any, array: any) {
    return (element.status != "AVAILABLE");
  }
  excludeUnavailable(element: Vehicle, index: any, array: any) {
    return (element.status != "UNAVAILABLE");
  }
  excludeLowBattery(element: Vehicle, index: any, array: any) {
    return (element.batteryLevelPct >= 30);
  }
  excludeMediumBattery(element: Vehicle, index: any, array: any) {
    return (element.batteryLevelPct < 30 && element.batteryLevelPct >= 70);
  }
  excludeHighBattery(element: Vehicle, index: any, array: any) {
    return (element.batteryLevelPct < 70);
  }
}
