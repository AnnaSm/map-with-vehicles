export class Vehicle {
    platesNumber!: string;
    sideNumber!: string;
    color!: string;
    type!: string;
    rangeKm!: number;
    batteryLevelPct!: number;
    status!: string;
    id!: number;
    name!: string;
    location!: Location;
}

export class Location {
    latitude!: number;
    longitude!: number;
}
