import { Component, AfterViewInit } from '@angular/core';
import * as L from 'leaflet';
import { FilterService } from '../filter.service';
import { MarkerService } from '../marker.service';
import { VehicleService } from '../vehicle.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})

export class MapComponent implements AfterViewInit {
  private map: any;

  constructor(private markerService: MarkerService, private vehicleService: VehicleService, private filterService: FilterService) { }

  private initMap(): void {

    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
      minZoom: 11,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    this.map = L.map('map', {
      center: [52.23209, 21.00600],
      zoom: 3,
    });

    tiles.addTo(this.map);
  }

  ngAfterViewInit(): void {
    this.initMap();
    this.vehicleService.getVehicles().subscribe(wrapper => {
      this.markerService.makeVehiclesMarkers(this.map, this.filterService.filterVehicles(wrapper.objects));
    });
  }
}